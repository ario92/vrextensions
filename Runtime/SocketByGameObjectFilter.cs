using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Filtering;
   
[AddComponentMenu("XR Extensions/Filters/Socket By GameObject", 1)]
public class SocketByGameObjectFilter : MonoBehaviour, IXRSelectFilter, IXRHoverFilter
{
    [SerializeField] private bool _canProcess = true;
    [SerializeField] private GameObject AcceptableGameObject;
    [SerializeField] private GameObject[] AcceptableGameObjectList;

    public bool canProcess => _canProcess;

    public bool Process(IXRSelectInteractor interactor, IXRSelectInteractable interactable)
    {
        if (interactable != null &&
            AcceptableGameObject != null &&
            interactable.transform.gameObject == AcceptableGameObject)
            return true;


        if (interactable != null &&
            AcceptableGameObjectList != null)
            foreach (var item in AcceptableGameObjectList)
            {
                if (item == interactable.transform.gameObject)
                    return true;
            }

        return false;
    }

    public bool Process(IXRHoverInteractor interactor, IXRHoverInteractable interactable)
    {
        if (interactable != null &&
             AcceptableGameObject != null &&
             interactable.transform.gameObject == AcceptableGameObject)
            return true;

        if (interactable != null &&
          AcceptableGameObjectList != null)
            foreach (var item in AcceptableGameObjectList)
            {
                if (item == interactable.transform.gameObject)
                    return true;
            }

        return false;
    }
}
